package alphabetsoup;

import java.util.List;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

/**
 * Unit test for WordGridTest
 */
public class WordGridTest
        extends TestCase {

    String fileLocation = "sample-alphabet-soup.txt";
    String fileLocation2 = "sample-alphabet-soup2.txt";

    /**
     * Create the test case
     *
     * @param testName name of the test case
     */
    public WordGridTest(String testName) {
        super(testName);
    }

    /**
     * @return the suite of tests being tested
     */
    public static Test suite() {
        return new TestSuite(WordGridTest.class);
    }

    /**
     * Test Setup
     */
    public void testWordGridSetup() {
        WordGrid wordGrid = new WordGrid(fileLocation);
        try {
            wordGrid.setup();
            List<String> searchWords = wordGrid.getSearchWords();

            assertFalse(searchWords.isEmpty());
            assertEquals(searchWords.size(), 3);
        } catch (Exception e) {
            // TODO: handle exception
        }
    }

    /**
     * Test Setup With Additional Words
     */
    public void testWordGridSetup2() {
        WordGrid wordGrid = new WordGrid(fileLocation2);
        try {
            wordGrid.setup();
            List<String> searchWords = wordGrid.getSearchWords();

            assertEquals(searchWords.size(), 4);
        } catch (Exception e) {
            // TODO: handle exception
        }
    }

    /**
     * Test Find search word across grid lines
     */
    public void testSearchAcross() {
        WordGrid wordGrid = new WordGrid(fileLocation);
        try {
            wordGrid.setup();
            String rtn = wordGrid.findSearchWord("GOOD");
            assertNotNull(rtn);
        } catch (Exception e) {
            // TODO: handle exception
        }
    }

    /**
     * Test Find search word diagonal grid lines
     */
    public void testSearchDiagonal() {
        WordGrid wordGrid = new WordGrid(fileLocation);
        try {
            wordGrid.setup();
            String rtn = wordGrid.findSearchWord("HELLO");
            assertNotNull(rtn);
        } catch (Exception e) {
            // TODO: handle exception
        }
    }

    /**
     * Test Find search word diagonal grid lines
     */
    public void testSearchWordBackwards() {
        WordGrid wordGrid = new WordGrid(fileLocation);
        try {
            wordGrid.setup();
            String rtn = wordGrid.findSearchWord("BYE");
            System.out.println(rtn);

            assertNotNull(rtn);
        } catch (Exception e) {
            // TODO: handle exception
        }
    }
}
