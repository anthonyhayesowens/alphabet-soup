package alphabetsoup;

/**
 * AlphabetSoup Word Search
 *
 */
public class App {
    public static void main(String[] args) {
        String fileLocation = args[0];
        WordGrid wordGrid = new WordGrid(fileLocation);

        try {
            wordGrid.setup();

            for (String word : wordGrid.getSearchWords()) {
                wordGrid.findSearchWord(word);
            }
        } catch (Exception e) {
            // TODO: handle exception
        }
    }
}
