package alphabetsoup;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class WordGrid {

    private String fileLocation;
    private char[][] wordGrid;
    private List<String> words;
    private int workGridRows;
    private int workGridColumns;

    public WordGrid(String fileLocation) {
        this.fileLocation = fileLocation;
    }

    public void setup() throws IOException {
        BufferedReader reader = null;

        try {
            reader = new BufferedReader(
                    new FileReader(this.fileLocation));

            String line = reader.readLine();
            String workGridColumn = line.substring(0, 1);
            String workGridRow = line.substring(2, 3);

            this.workGridRows = Integer.parseInt(workGridRow);
            this.workGridColumns = Integer.parseInt(workGridColumn);

            this.setWordGrid(reader, Integer.parseInt(workGridRow));
            this.setWords(reader);
            reader.close();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (reader != null) {
                reader.close();
            }
        }
    }

    public List<String> getSearchWords() {
        return this.words;
    }

    private void setWords(BufferedReader reader) throws IOException {
        String line;
        this.words = new ArrayList<String>();
        line = reader.readLine();
        while (line != null) {
            this.words.add(line);
            line = reader.readLine();
        }

    }

    private void setWordGrid(BufferedReader reader, int workGridRows) throws IOException {

        int currentLine = 1;
        String line;
        this.wordGrid = new char[this.workGridRows][this.workGridColumns];

        while (currentLine <= workGridRows) {
            line = reader.readLine();
            line = line.replaceAll("\\s", "");

            this.wordGrid[currentLine - 1] = line.toCharArray();
            currentLine++;
        }

    }

    public String findSearchWord(String searchWord) {
        String rtn = null;

        String location = getLocation(searchWord);
        if (location != null) {
            System.out.printf("%s %s \n", searchWord, location);
            rtn = "Found " + searchWord;
        }

        return rtn;
    }

    public String getLocation(String searchWord) {
        String position = null;

        char[] word = searchWord.toCharArray();

        for (int rowIndx = 0; rowIndx < this.workGridRows; rowIndx++) {
            for (int colIndx = 0; colIndx < this.workGridColumns; colIndx++) {
                if (word[0] == this.wordGrid[rowIndx][colIndx]) {
                    position = this.findWord(word, rowIndx, colIndx, 1, 1);
                    if (position != null) {
                        return position;
                    }

                    position = this.findWord(word, rowIndx, colIndx, +1, -1);
                    if (position != null) {
                        return position;
                    }

                    position = this.findWord(word, rowIndx, colIndx, -1, +1);
                    if (position != null) {
                        return position;
                    }

                    position = this.findWord(word, rowIndx, colIndx, -1, -1);
                    if (position != null) {
                        return position;
                    }

                    position = this.findWord(word, rowIndx, colIndx, 0, 1);
                    if (position != null) {
                        return position;
                    }

                    position = this.findWord(word, rowIndx, colIndx, 0, -1);
                    if (position != null) {
                        return position;
                    }

                    position = this.findWord(word, rowIndx, colIndx, 1, 0);
                    if (position != null) {
                        return position;
                    }
                }
            }
        }

        return position;
    }

    private String findWord(char[] word, int rowIndx, int columnIndx, int rowDirection,
            int columnDirection) {
        int row = rowIndx + rowDirection;
        int column = columnIndx + columnDirection;
        boolean wordFound = true;
        int foundRow = row;
        int foundColumn = column;

        try {
            for (int wordIndx = 1; wordIndx < word.length; wordIndx++) {
                if (word[wordIndx] == this.wordGrid[row][column]) {
                    foundRow = row;
                    foundColumn = column;
                    row = row + rowDirection;
                    column = column + columnDirection;
                } else {
                    wordFound = false;
                }
            }
        } catch (Exception e) {
            wordFound = false;
        }
        return (wordFound) ? rowIndx + ":" + columnIndx + " " + foundRow + ":" + foundColumn : null;
    }
}
